﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Multithreading
{
    class Program
    {
      
        static async Task Main(string[] args)
        {

          
            var data100k = new TestData(100_000);
            var data1m = new TestData(1_000_000);
            var data10m = new TestData(10_000_000);
            
            Console.WriteLine("===================SimpleSum===================");
            data100k.SimpleSum();
            data1m.SimpleSum();
            data10m.SimpleSum();
            
            Console.WriteLine("===================ParalleSum===================");
            data100k.ParalleSum();
            data1m.ParalleSum();
            data10m.ParalleSum();

            Console.WriteLine("===================PLINQSum===================");
            data100k.PlinqSum();
            data1m.PlinqSum();
            data10m.PlinqSum();

            Console.WriteLine("===================TasksSum===================");
            await data100k.TasksSum();
            await data1m.TasksSum();
            await data10m.TasksSum();

            Console.ReadLine();
        }


    }
}
