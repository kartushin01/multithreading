﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Multithreading
{
    public class TestData
    {
        public int Count { get; set; }
        public int[] Data { get; set; }

        public TestData(int count)
        {
            int[] arr = new int[count];
            this.Count = count;
            this.Data = arr;
        }

        public void SimpleSum()
        {
            Stopwatch time = new Stopwatch();

            time.Start();
            int result = 0;

            result = this.Data.Sum();

            var stop = time.Elapsed.TotalMilliseconds;

            Console.WriteLine($"Array count {this.Count}| Sum: {result} of Simple time: {stop}");
        }

        public void ParalleSum()
        {
            Stopwatch time = new Stopwatch();

            time.Start();

            int result = 0;

            Parallel.ForEach(
                this.Data,
                () => 0,
                (n, loopState, localSum) =>
                {
                    localSum += n;
                    return localSum;
                },
                (localSum) => Interlocked.Add(ref result, localSum)
            );

            var stop = time.Elapsed.TotalMilliseconds;

            Console.WriteLine($"Array count {this.Count}| ParalleSum time: {stop}");
        }

        public void PlinqSum()
        {
            Stopwatch time = new Stopwatch();

            time.Start();

            long sum = this.Data.AsParallel().Sum();

            var stop = time.Elapsed.TotalMilliseconds;

            Console.WriteLine($"Array count {this.Count}| PlinqSum time: {stop}");
        }

        public async Task TasksSum()
        {

            Stopwatch time = new Stopwatch();

            time.Start();

            var coresCount = Environment.ProcessorCount;
            var chunk = this.Data.Count() / coresCount;
            var tasks = new List<Task<int>>();

            for (var i = 0; i < coresCount; i++)
            {
                tasks.Add(Task.Run(() => this.Data.Skip(chunk * i).Sum()));
            }

            await Task.WhenAll();

            int sum = tasks.Select(task => task.Result).Sum();

            var stop = time.Elapsed.TotalMilliseconds;

            Console.WriteLine($"Array count {this.Count}| TaskCalc time: {stop}");
        }


    }
}
